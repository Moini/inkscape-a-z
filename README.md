# *Inkscape von A bis Z*: Wichtige, unerwartete und spannende Features im Überblick

Vortrag für die Tux-Tage 2021 (13. November 2021, 16:30 Uhr, 45 min)

Copyright: CC-By-SA 4.0 Maren Hachmann, Inkscape-Logo von Andrew Michael Fitzsimon
